# Viktor Kasimir

## Personal information:

Email: vikkas-4@student.ltu.se  
Mobile number: +46 73 843 70 88


## Some information about myself

I am a down to earth guy in his best years! Loves to workout, primarily rock climbing but I also windsurf and mountainbike when i get the opportunity.  
Live alone in my apartment and have studied every control course there is at LTU in my five years I been living in Luleå.  

Even though I study to become an engineer I have started to study psychology on my free time because people and personalities intrigue me. Also the psychology of flow as a method for reaching ultimate experience in everything I do.  

My current work-goal is first to finish my studies and work as an engineer for a couple of years to get to know the industry, then become project manager because that is something i find very fun and challenging! Later in life i might consider myself as a consultant and by then i should have enough experience for the role.

## Role in project

My role in the project is primarily management of tasks and making sure we are following the project plan. This is done by following a Gantt chart and planning weekly tasks from this Gantt chart in Trello which the group will work from.  
I am also managing the git repository, making sure everyone in the project keeps it clean and knows how the workflow is supposed to work.  
Lastly I will work together with the other control people in the group to develop models and control algorithms for the project

My Background is engineering physics with a focus for control theory. I have also previously studied embedded system programming and system identification courses at Luleå university of technology. 