# Adonay Migsina

## Contact information
* adomis-2@student.ltu.se
* 073-8135079

## Some information about myself
I like dumpster diving and making a "Frank Einsteins monster" of what i find. I like playing with GNU/Linux.

# My role in the project
- Responsible for the report.
- Modeling part of the wheelloader - the arm and bucket.
- Share work for the hardware related things.
- Share work for the software related things software.

