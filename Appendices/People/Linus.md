# Linus Karlquist

## Personal infomation

mail: iluaku-5@student.ltu.se

Phone number: 070-746 36 06

## Some information about myself

I am a calm and quiet person who likes to take it easy. Because of this i like to lie on the sofa watching movies and tv-series and also sitting in my armchair in front of the computer. When not just browsing the web i like to play video games, in particular car simulators. 

In my spare time I drive RC-cars in a couple of different classes and have also done some racing in this. The reason that I drive car simulators and RC-cars is that I am interested in cars and basically anything with an engine. Because of this I also drive motocross. 

I rock climb to get some exercise and in the summer I like to waterski. 


## My role in the project

In this project I am mainly acting as a software engineer. I will work on the ROS network and all other connections between the controllers and sensors/actuators. 

I will also work on the hardware, mounting everything to the wheel loader and making the connections. 

Even though I am not a part of the development of the control for the wheel loader it will be interessting to learn how it is done.  
