# Emil Ollars

## Personal infomation
mail: emioll-4@student.ltu.se

Phone number: 072-5880460

## Some information about myself
I like food, both eating and making it. On Fridays I enjoy getting together with some friends, grabing a couple of beers, and making dinner together. As a hooby I've been playing the bass for almoast a decade, I play electric bass as well but recently I've been focusing mainly on the double bass. 

I love learning new things and I'm also a fast learner. Lately I've been really into language studies. When I was rather new at the university I picked up Japansese, and have been studying it intensively ever since. I now speak it at a quite comfortable level and I reacently came back from my semster abroad in Japan.

My background is engineering physics and electroincs, I've done both some control and some hardware and I'm currently trying to figure out which one I like the most.  


## My role in the project

In the project I want to learn about software, in this case ROS and how to comunicate via a network. I will also help out with the control theory.   