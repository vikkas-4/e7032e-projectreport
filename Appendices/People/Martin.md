# Matin Norén

## Personal infomation
mail: amionq-4@student.ltu.se

Phone number: 072-870 3735

## Some information about myself
Music is without question the thing in life that is most important for me, I have played guitar for the best part of my life and love to record music in cubase. But I hurt my shoulder in the gym some 10 years ago and it never got good after that. Now I can only play for a limited time every day before It starts to hurt. That is why I started to study engineering, I gave up on hoping the body magically would heal on day, better to find another way to earn a living. And now in hindsight I'm really happy with it all, I don't think I ever would have found working with music to be completely fulfilling, I really love technology and the progress in all fields of science. Having music in live purely for enjoyment is perfect for me. 

Other than that I love movies, have seen many thousands over the years, playing games, VR , drawing and cooking food. To be honest, I think I would really like anything if I just put some time into it, biology, geology.. maybe not sports though hah.


## My role in the project

What I mainly want to get out of the project is to understand the connection of the actuators and servo etc to the Arduino and the code to make it work. We have done similar things on projects before, but I had other task then and missed out on the fun. 
I also think It will be interesting to get our custom hardware to fit on the wheel loader, see what we can come up with to make it look somewhat professional.
I have a part in the control of the machine, mainly because a suck at it and want to see if I can learn it, I will probably pester my team mates to help me a lot with that part.


