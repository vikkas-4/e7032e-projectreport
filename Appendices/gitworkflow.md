# Git workflow

1. Make shure you're up to date 
	* git pull
	
2. Make a new branch
	* git branch branch_name

3. Enter the new branch
    * git checkout branch_name

	
3. Do work...
	* git add -A
	* git git commit -m "comment"
	* git push
	
4. Crate merge request at [Gitlab.com/vikkas-4/e7032e-projectreport/"YOUR_BRANCH"](https://gitlab.com/)
	
![Crate merge request](merge.png)
5. Have your merge request approved by a friend :D

