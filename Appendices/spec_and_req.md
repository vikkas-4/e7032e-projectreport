# System Specification and Requirements

## Introduction
The wheel loader design is a concept design from Volvo CE. Volvo CE  have developed a concept on how a fully autonomous wheel loader might look like in the future. In collaboration with Lego they have developed a small scale model for the wheel loader in Lego.  

This Lego wheel loader is augmented with actuators, sensors and control systems to be able to drive and scoop just like a real wheel loader.

## Intended use
This robot Lego wheel loader is intended to be designed such that a AI could be implemented, controlling the motion of the wheel loader. 

An AI have been developed by "insert name" at Luleå University of Technology. Our aim is to adapt the sensor and actuation signals used to design this AI to the LEGO wheel loader so that the AI could possibly be implemented in the future.

## Requirements
This section covers the requirments needed to reach the specification goal.

### Drive
The wheelloader should be able to drive forward, backards and also turn. This should then be controlled by a controlsystem. The controlsystem is covered in another section while this section focus on the Hardware needed.

#### Motors
The motors should be sufficiently powerful to drive the wheel loader forward. The motor speed should be controlled by a microcontroller.
between the microcontrlller and the motor a motorcontroller circuit is implemented to control the direction of rotation and the speed of the motor. This is to easily control the motor dynamics using a microcontroller.

#### Encoders
For control purposes feedback of the speed is needed.  This is why encoders should be implemented to measure the speed of the wheel loader when moving forward or backwards. The encoder signals is fed into the microcontrollers to then calculate the speed to be used in the control agorithm.

####  Steering
Using a servo motor the steering is controlled by the microcontroller. No steering control is implemented by the reasearch team acording to their paper but steering is necessary for this projects since we want to be able to move around using a joystick. 

### Scooping Arm
The scoop and the arm should be controlled by motors sufficiently powerful such that the arm can scoop objects of a certain weight. 
For control purposes encoders are used to feed back the position and velocity data of the arm and scoop. Also a current sensor should be used to measure the currentdraw of the motors in order to be able to estimate the liftforce needed to be applied.

### Control System
Control systems is needed to be able to track the signals sent by the AI in a good way.
This section covers the criterias for these control algorithms and what each controller should be able to perform.

#### Drive
The drive control should be implemented such that the reference sent in by the AI can be tracked in a desirable way. 
Criterias for desirable reference tracking:
* Track the reference fast enough
* Robust against disturbances and noise

#### Scoop and Arm
The arm and scoop control shold be implemented such that the reference sent by the AI can be tracked in a desirable way. 
Criterias for desirable reference tracking:
* Track the reference fast enough
* Robust against disturbances and noise

### Networking and PC Communication
This section covers the commmunication protocols used in order to communicate between each component.

#### I2C
The currentsensor talk through I2C communication. This is inorder to ensure good accuracy when measuring the currentdraw from the motors.

#### USART
The microcontrollers can easily communicate between eachother using USART. This communication is needed to be able to send sensordata back to the AI but also to recieve commands and references for the controllers implemented on the microcontrollers.

#### ROS
A ROS network is used to communicate with the AI on a computer. This is a way to send the sensordata to the AI so it can make decisions and the send back the references to the microcontrollers.