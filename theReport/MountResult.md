
# Mounting



## Wheel mount



Here we see the wheel mount screwed on place.


<figure>

<img src="pictures/img/wheelmount1.jpg" width="550">
<figcaption>Wheel mount screwed onto old Lego wheel
piece.<figcaption>
</figure>

In order to connect the motor to the wheel, the cross-shaped hole of the Lego wheel was filed to fit the half-moon shaped motor. This way of connection is very hard to make completely straight which resulted in the wheels wobbling when turning.

<figure>
<img src="pictures/img/wheelMotorConnection.jpg" width="550">
<figcaption>Motor connection to wheel.<figcaption>
</figure>


## Bucket mount



The hanging design showed to be very stable, the extra hole on the side was not needed for stability.

<figure>
<img src="pictures/img/bucketMount.jpg" width="550">
<figcaption>The finished bucket mount.<figcaption>
</figure>

Here the modified connector is used to connect the motor to the wheel loader. Because of the half-moon shape on the motor sticking into the connector the whole bucket mount wobbles each rotation, this will not, however, hinder the bucket to move in any way.

<figure>
<img src="pictures/img/bucketMountOnArm.PNG" width="550">
<figcaption>The bucket mount, mounted onto the arm.<figcaption>
</figure>

## Arm mount


The complete arm mount's connection is here demonstrated with the "hydraulic" piston removed from the wheel loader for clarity.

<figure>
<img src="pictures/img/ArmMount2.jpg" width="550">
<figcaption>Arm mount removed.<figcaption>
</figure>

<figure>
<img src="pictures/img/ArmMount1.jpg" width="550">
<figcaption>Arm mount connected.<figcaption>
</figure>


The arm mount is also wobbling when turning because of the half-moon shape of the motor.

<figure>
<img src="pictures/img/armMountOnpiston.jpg" width="550">
<figcaption>Arm mount on fitted onto mechanical piston barrel.<figcaption>
</figure>

## Motor to wheel loader connection

In order for the motors to turn the knobs moving the bucket and arm, a connector needs to be used. 

The first 3D-printed connectors were too weak and broke apart. This design was discarded in favor of the moded lego connectors. 

<figure>
<img src="pictures/img/Bild1.jpg" width=450">
<figcaption>First version of the connectors that were to weak and broke apart.<figcaption>
</figure>


The new design was made by filing one side of an original Lego connector to fit the half-moon shape of the motor. It was hard to make this symmetrical because of the nature of the half-moon shape. This makes the axis a bit out of alignment making the motors wobble a bit when turning.


<figure>
<img src="pictures/img/armAndBucketmotorconnector.jpg" width="550">
<figcaption>Connectors for arm and bucket.<figcaption>
</figure>


## Servo mount

Final assembly of the servo onto the bottom of the wheel loader.

<figure>
<img src="pictures/img/servo.jpg" width="550">
<figcaption>Servo under the chassis with tape on pins. Glue can be seen above and
below the blue pins.<figcaption>
</figure>