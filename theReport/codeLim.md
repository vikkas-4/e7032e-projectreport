# Limitations of the code

## Data loss
The STM32 expects each transmission to contain three bytes so when sending orders without values i.e. "hello", "connected" etc. the Raspberry Pi sends the order three times.
A possible problem is that the MCU waits until it has read three bytes. 
If the the raspberry pi was to only send two bytes, or if some data was lost when transmitting the order, the STM32 will lock down. 
To prevent this the MCU should have a time limit were if it doesn't receive the full three bytes within a certain time it will stop. 
Currently it doesn't have this function but it's something we consider implementing.   
