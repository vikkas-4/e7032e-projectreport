# Introduction

It all started in a teambuilding event that Volvo CE and LEGO Technic design teams had and this led to a concept of a wheel loader that it is battery driven, flexible (more degrees of freedom) and fully autonomous. This is a type of vehicle that would fit in the mining industry. The idea was developed with help from children and the result become the _Volvo Zeux_ 

The most interesting parts and characteristics of the Zeux are: _the mapping drone_, _the eye_, it is fully electric, the moving _counter weight_, the moving _scissor frame_ and, the hub wheels and the Ackerman steering on both of its axis.

_The mapping drone_ hovers above the wheel loader and sees the surrounding. _The eye_ is a robot arm placed on the top of the wheel loader with a camera on its edge and functions as sort of "eye contact" with the humans around it and indicates where the wheel loader is heading. So the wheel loader drives towards the direction _the eye_ is looking at. The vehicle is fully electric equipped with 150kWh battery which it can use for three to four hours and each of its four wheels is driven with a 25kW hub-wheel.

Zeux weight is relative low so while lifting the load in its bucket, it would move it's heaviest part called _the counter weight_ which is the part where the batteries are placed backwards by sliding it back and upwards by moving it's scissor frame up in order to keep the center of mass in the center and prevent from being tipped forward due to the load. The Ackerman steering on both of its axis makes the Zeux to turn large angles and still be stable. The four hub wheels are equipped with sensors which provides with feedback input of the _[rim-pull effort](https://www.easycalculation.com/maths-dictionary/rimpull.html)_ (the amount of force exerted at the point where the vehicle touches the road surface) that helps the wheel loader keep its balance and performs it's task efficiently. 

LEGO Technic developed a LEGO prototype of this wheel loader and its name is _LEGO Technic 42081 Volvo Concept Wheel Loader ZEUX_ Currently this is the only version of the _Volvo Zeux_ that exists. The design and concept of the full sized version of the wheel loader is called _Volvo Concept Wheel Loader Zeux_ Now being beginning of 2020, there is no full sized Zeux wheel loader manufactured yet.

This report is part of the course _Project in Engineering Physics and Electrical Engineering - Electronic Systems and Control Engineering (E7032E)_ and summarizes the work done making a _LEGO Technic 42081 Volvo Concept Wheel Loader ZEUX_ prototype fully autonomous. The wheel loader came with neither actuators nor sensors. So everything from making it move and controlling it was done in this project. The plan was from the beginning to make it fully autonomous and connect it to an AI that would be its operator. But due to lack of time, it is not made fully autonomous in this project. But all work on the wheel loader is done with consideration that one sometime in the future, someone may work on making the same wheel loader fully autonomous and perhaps with a will on making it work according to the _Volvo Concept Wheel Loader Zeux_'s design. 

By the end of this project, not all of the moving parts of the LEGO wheel loader are kept. The _scissor frame_ does not move up and down and the _contra weight_ does not move back and forward either. This is mostly because they were preserved ass less prioritized tasks and all focus was put on the more essential parts of the project and perhaps if there is time left after finishing this parts, move on to the parts with less priority. But as the wheel loader consists mostly of LEGO parts and hence has great modularity, it is easy to implement the moving parts back. ;)


--- ---


- Apendix for this part of the report:


    - Source links:
    
    [Link to the Volvo CE site describing the Zeux Concept](https://www.volvoce.com/global/en/this-is-volvo-ce/what-we-believe-in/innovation/zeux/)
    
    [Link to the LEGO site selling the Zeux Concept LEGO model](https://www.lego.com/en-us/product/volvo-concept-wheel-loader-zeux-42081)


    - Picutures of the Zeux-Concept:

<figure>
<img src="pictures/Zeux_Figures/Zeux_001.png">
<figcaption> <figcaption>
</figure>

<figure>
<img src="pictures/Zeux_Figures/Zeux_002.png">
<figcaption> <figcaption>
</figure>

<figure>
<img src="pictures/Zeux_Figures/Zeux_003.png">
<figcaption> <figcaption>
</figure>

<figure>
<img src="pictures/Zeux_Figures/Zeux_004.png">
<figcaption> <figcaption>
</figure>




