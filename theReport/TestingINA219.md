<!-- Inserting figures -->
<!-- Figure nr -->

<!-- https://cdn-learn.adafruit.com/downloads/pdf/adafruit-ina219-current-sensor-breakout.pdf --> 
<!-- http://www.ti.com/lit/ds/symlink/ina219.pdf --> 

# Documentation of Testing the INA219 Current Sensor

## Test Measurement Nr. 1

A test on the current sensor was done using the Adafruit INA219 example code in Arduino. In this test, a calibration of 16V and 400mA from the ”Adafruit_INA219” library was used.

Current from a switching adapter with output of 5V and 1800mA (max. 9W) was measured using the current sensor. The current was measured through a resistor of 0 ohm, 10 ohm, 20 ohm and 30 ohm to see if the expected current through the resistors and measured value by the INA219 and a multimeter match. In this specific test, The results do not match. It is probably because of the calibration. Later on, correct calibration will be used.

The connections was done as shown in figures (1) and (2).



| ![CircuitDiagram](pictures/TestingINA219/LowSideMeasurementDiagram-01.png "Current testing diagram 1") |
| :--: |
| *Figure 1: Current measurement diagram: low side sensing* |

| ![CircuitDiagram](pictures/TestingINA219/TestCircuit-01.jpg "Test circuit 01") |
| :--: |
| *Figure 2: Test circuit 01* |


The current sensor is though measuring on the low side of the circuit in this test. The design is to measure from the high side.

Measuring various loads on the source with capacity of 5V and 1800 mA and having callibrated the arduino code for the INA219 sensor for 16V_400mA, the following results could be achieved given load of 0, 10, 20, 30 ohm and open circuit respectively. 
  
| Load: | 0 ohm | 10 ohm | 20 ohm | 30 ohm | inf. ohm |
|---------| :---------: | :---------: | :---------: | :---------: | :---------: |
| Bus Voltage: | 0.00 V | 0.00 V  | 0.00 V | 0.00 V | 0.00 V |
| Shunt Voltage: | 0.03 mV |  40.00 mV | 26.20 mV | 17.72 mV | -0.01 mV |
| Load Voltage: | 0.00 V |  0.04 V | 0.03 V | 0.02 V  | -0.00 V |
| Current: | 0.30 mA | 400.00 mA | 262.00 mA | 177.10 mA | 0.00 mA |
| Power: | 0.00 mW | 0.00mW | 0.00 mW | 0.00 mW | 0.00 mW |

---

## Test Measurement Nr. 2


| ![CircuitDiagram](pictures/TestingINA219/HighSideMeasurementDiagram-01.png "Current testing diagram 2") |
| :--: |
| *Figure 3: Current measurement diagram: high side sensing* |
