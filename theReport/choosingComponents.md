# Choosing components



For easier logistics and simplicity of the wheel loaders, all motors were chosen from the motor with maximum torque requirement. The same kind of motors with decodes were in the same price range regardless of power output.


Testings showed that the torque to turn the bucket's movement knob when the bucket is loaded to maximum capacity will decide the power required of the motors on the wheel loader, this is the motor that requires most torque to move.


## Test for motor power requirement


The bucket was weighted with 0.3 kg which is the wheel loaders maximum lift weight before the arm structure starts to deform.


A connection tool was mounted to the movement knob to be able to use a Newton meter.


Pulling on the Newton meeter provides a reading of how much force is required to turn the knob that moves the arm or bucket respectively.



| Material        | Dimensions |
| --------------- | ---------- |
| Connection tool | 0.04m      |
| Newton meter    |            |
| Bottle          | 0.3kg      |
| Wheel loader    |            |

From the test result below we see that it takes almost 3 times more force to turn the knob to move the bucket, than turning the knob for the arm.


Loader arm and scoop force measurements.


| Reading        | 1    | 2    | 3    | 4    |- Mean |
| -------------- | ---- | ---- | ---- | ---- | ---- |
| Arm weighted   | 0.25 | 0.20 | 0.25 | 0.22 |- 0.23 |
| Scoop weighted | 0.6  | 0.7  | 0.6  | 0.7  |- 0.65 |

The torque needed for the motors was then obtained by using Equation below.

l_arm * N = Nm => 0.04*0.65 = 0.026

The motor that was readily available for us had a torque of about 0,980665 witch is far more than what is needed for the wheel loader.


<figure>
<img src="pictures/connection tool.jpg" alt="RasPi" width="350">
<figcaption>Connection tool to connect newton meter with wheel loader.<figcaption>
</figure>

<figure>
<img src="pictures/Newton Meeter.jpg" alt="RasPi" width="350">
<figcaption>Newton meter measuring force require to turn the knob.<figcaption>
</figure>

<figure>
<img src="pictures/test setup - Bright.jpg" alt="RasPi" width="350">

<figcaption>The test setup.<figcaption>
</figure>

<figure>
<img src="pictures/TestR2.jpg" width="350">
<figcaption>The knob to turn to move the bucket.<figcaption>
</figure>

