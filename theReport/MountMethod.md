# Mounting 

All of the mounts except for the servo was designed using CAD with the program inventor. These 3d models were then exported to sdl files that a 3D printer could read and use to print out the mounts. All designs were made so that they could be used without any adhesive.

The sketches are not complete and correct blueprints but a way to get a clearer understanding of the problem before starting Inventor.

The Inventor blueprint pictures have been added to get a better view of the design. They have no measurements because they are not to be used as blueprints for CNC machines or similar manufacturing techniques.

All individual planes have the measurements in the CAD Inventor program, an example of one of these planes is given for the arm mount below.

The process to get the right measurements for the Inventor CAD's was to first make sketches using calipers on the Lego wheel loader and calculations using measured from standard Lego distances. These sketches were then the base for the Inventor designs.

<figure>
<img src="pictures/img/lego.png" width="550">
<figcaption>Standard Lego distances.<figcaption>
</figure>



## Weel Mount


The wheel mount is the basis for all other mount sins this has the design that will hold the motor and at the same time can be screwed onto other Lego parts.


A quick sketch was made to figure out how to fit the motor and at the same time be able to be fitted to the Lego part that the old Lego wheel was placed onto.


The mounts for the wheels are connected to the Lego structure by drilling 4 holes into the original wheel mounts and screwed into place.



<figure>
<img src="pictures/img/WheelMountDesign.png" width="550">
<figcaption>Wheel mount first draft design sketch.<figcaption>
</figure>



The two holes on the front will hold the motor with screws, and the four on the back are made to connect to the old wheel holder.



<figure>
<img src="pictures/img/WhelMountCAD.PNG" width="550">
<figcaption>Wheel mount final CAD Inventor design.<figcaption>
</figure>



<figure>
<img src="pictures/img/MotorToWheelDesign.png" width="550">
<figcaption>sketch of the Lego wheel holder.<figcaption>
</figure>



<figure>
<img src="pictures/img/MotorToWheelDesign2.png" width="550">
<figcaption>Front sketch of the Lego wheel holder with measurements for the four holes that fits the wheel motor mount.<figcaption>
</figure>



<figure>
<img src="pictures/img/WheelMountSCEM.png" width="550">
<figcaption>Wheel mount final CAD Inventor blueprint.<figcaption>
</figure>



## Bucket Mount


In order to connect the bucket motor to the Lego wheel loader, this design was sketched to see if it would be possible to hang it in the Lego holes from the top of the arm. It's easy to see here that the wheel mount is used as base for this bucket mount also.


<figure>
<img src="pictures/img/BucketMountDesign.jpg" width="550">
<figcaption>Bucket mount first draft design sketch.<figcaption>

</figure>


The hole on the flat surface was made to add one extra holding place for the mount if need.


<figure>
<img src="pictures/img/BucketMountCAD.PNG" width="550">
<figcaption>Bucket mount final CAD Inventor design.<figcaption>
</figure>



<figure>
<img src="pictures/img/BucketMountSCEM.PNG" width="550">
<figcaption>Bucket mount final CAD Inventor blueprint.<figcaption>
</figure>


## Arm Mount


Creating the arm mount was presented with an extra complication sins the only place to place the mount was directly onto the piston cover moving the arm. This is because the turning knob for moving the arm moves when the arm moves shown in the pictures below.


<figure>
<img src="pictures/img/Problem1.jpg" width="550">
<figcaption>Arm connection position 1.<figcaption>
</figure>



As we see the turning know has moved when the bucket has been raized into the air.


<figure>
<img src="pictures/img/Problem2.jpg" width="550">
<figcaption>Arm connection position 2.<figcaption>
</figure>

This resulted in a very complicated design where both holes in the lego and rods sticking out from the lego was needed to fasten the mount securely. This can be seen in the result of the arm mount.


<figure>
<img src="pictures/img/ArmMountDesign1.jpg" width="550">
<figcaption>Arm mount first draft design sketch.<figcaption>
</figure>



<figure>
<img src="pictures/img/armMountCad.PNG" width="550">
<figcaption>Arm mount final CAD Inventor design.<figcaption>
</figure>


<figure>
<img src="pictures/img/ArmMountSchematics.png" width="550">
<figcaption>Arm mount final CAD Inventor blueprint.<figcaption>
</figure>


## Motor to wheel loader connection


This was the first plan to connect the motors to the wheel loader, a 3d printed connector designed to fit the half-moon shaped motor side with the cross shape of the lego.




<figure>
<img src="pictures/img/ConnectorDesign.jpg" width="550">
<figcaption>Abandoned motor to wheel loader connection first draft design sketch.<figcaption>
</figure>


<figure>
<img src="pictures/img/Con3.PNG" width="550">
<figcaption>Abandoned motor to wheel loader connector final CAD Inventor design.<figcaption>
</figure>


<figure>
<img src="pictures/img/legoTillMotorKopplingSCEM.png" width="550">
<figcaption>Abandoned motor to wheel loader connector final CAD Inventor blueprint.<figcaption>
</figure>



<figure>
<img src="pictures/img/AbandonedConnectorCAD.PNG" width="550">
<figcaption>Abandoned design connector pretruding CAD Inventor design.<figcaption>
</figure>


<figure>
<img src="pictures/img/AbandonedConnectorSCEM.png" width="550">
<figcaption>Abandoned design Connector pretruding blueprint.<figcaption>
</figure>


This idea was abandoned for the 3d printer does not have the precision to make that small details and the resulting prints were prone to breakage.

Instead, an original Lego connector was modified on one end just by filing a half-moon into one side fitting the motor.


<img src="pictures/img/YellowConnector.PNG" width="550">
<figcaption>Original Lego piece used to make final motor to Lego connector (not yet modified).<figcaption>
</figure>





## Servo Mount


The servo moves the wheels by pushing two pegs in ether direction. In order to diminish hysteresis, the width of these pegs needed to be thicker so that gap would be as small as possible when the direction changes. Ordinary office tape was used for this witch all but removed the gap. But even the plastic of the tape's glossy side have a grippy surface that will make the servo arm chop a bit into new positions when moving instead of turning smoothly. The effect on the wheel loader are small but adding lubricant to the tape would make the problem smaller. Sins the servo was a few millimeters too high for the ordinary Lego parts, some hot glue was used to secure the servo sins it is hanging upside down.



<figure>
<img src="pictures/img/ServoDesign.jpg" width="550">
<figcaption>Servo mount first draft design sketch.<figcaption>
</figure>

