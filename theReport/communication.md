# Communication


## The code

The code can be found in the E7032E-LegoWheelLoader [repository](https://gitlab.com/vikkas-4/e7032e-legowheelloader). 
In this section, the communication chain from the PC to the Raspberry Pi to the Nucleo is explained by examining the two python programs *ai\_talker.py* and *pi\_listener*.
Sending sensor data from the Nucleo back to the PC is done in the same manner but with the publisher and subscriber reversed.

The ai\_talker node program has two functions and one class. 
The `main()` function starts the ROS-node and initializes the order-variables. 
The `talker()` function takes input from the controller and publishes them on the *order\_topic* continuously.

<figure>
<img src = "pictures/ailistener.png" width = 650>
<caption>Flowchart of the ai_talker node.
</caption>
</figure>

The pi\_talker node program has three functions and the one class. 
The `main()` function starts the ROS-node, initializes the order-variables and opens up the serial ports. 
The `handshake()` function runs a single time on startup to make sure that data can be sent and received properly. 
Once the setup is done the program loops through the `listener()` function which is waiting to receive data from the *order\_data*. 
Every time a message is received the `callback()` function is called, this function sends the data to the Nucleo via the serial port.  
 

<figure>
<img src = "pictures/pilistener.png" width = 650>
<caption>Flowchart of the pi_listener node.
</caption>
</figure>

### The Order class

```python
class Order:
    def __init__ (self, order, sign, value):
    """ Predefined Order class that facilitates the communication between the RasPi and the MCU,
        the orders are defined as numbers between 0-10 and represented with the corresponding binary number.
        i.e. servo = 2 = b'/x10' 
    """
        
        self.order = order
        self.sign = sign
        self.value = value

    def write(self):
	""" This function writes an Order class object to the MCU.
	    standard procedure to write a message is:
		1) Update the objects fields with the desired values
			self.order = ...
			self.sign = ...
			self.value = ...
		2) Call  "self.write()"
	"""

        msg = bytearray()   		# Create a message to send to the MCU, this should be a bytearray = [order, sign, value]   
        msg.append(self.order)		# Add the values to the bytearray
        msg.append(self.sign)
        msg.append(self.value)

        if self.order == 4 or self.order == 5:
            ser_arm.write(msg)  	# Write the packaged message to the MCU
        else:
            ser_drive.write(msg)
        return
```

The Order class is defined at the beginning of both the *ai_talker.py* and the *pi_listener.py* scripts. This class has three fields and one function. The fields are used to save all information concerning a specific actuator in the same variable.  The following five Order-class variables are defined in the `main()` function.

```python
servo = Order(2, 0, 0)
drive = Order(3, 0, 0)
motor_arm = Order(4, 0, 0)
motor_bucket = Order(5, 0, 0)
stop = Order(8, 0, 0)
```

The `.write()` function compiles the order, sign and value of the variable into a byte array of size 3 uses the if-else structure to make sure that the message is sent to the proper Nucleo. The motor\_arm and motor\_bucket (order 4 and 5) are sent to the second Nucleo board while the rest of the orders are sent to the first Nucleo.  



### Node setup 

The ROS nodes are set up and initialized in the following *listener()* and *talker()* functions. 
The functions below are taken from the *AI_talker.py* and the *AI_listener.py* files and are used as an example.   
The *Pi_talker* and *Pi_listener* nodes are initialized in the same way on the Raspberry Pi.  

#### AI_talker.py:

```python
...
def talker():
	pub = rospy.Publisher('order_topic', order_msg, queue_size=10)
	rospy.init_node('ai_talker', anonymous=True)
	r = rospy.Rate(20)
...
	pub.publish(order, sign, value)
```

The `rospy.Publish()` function defines how the node publishes data. 
In this case, the node publishes a message of type *order_msg* to the *order_topic* topic. 
The *queue_size* parameter sets a maximum amount of messages that can be queued at a time. 
One consideration is that the queue size needs to be big enough to fit all messages that are being sent while not being too large. 
A larger queue size will naturally queue up a lot of messages if a subscriber is lagging behind, this can introduce significant latency. 
A queue size of 10 is a good middle ground but this value could probably be adjusted to improve the performance.

`rospy.init_node()` starts a node called 'ai_talker'. 
The `anonymous=True` argument gives the node a unique ID and allows multiple of the same node to be run simultaneously.

`rospy.rate()` sets the publishing rate in Hz. 

With these settings the `pub.publish(order, sign, value)` function takes three arguments and publishes them as an *order_msg* to the *order_topic* at a rate of 20 Hz.


#### PI_listener.py:

```python
...
def listener():
    rospy.init_node('pi_listener', anonymous=True)
    rospy.Subscriber("order_topic", order_msg, callback)
    rospy.spin()	
...
```

`listener()` is the equivalent of `talker()` but for subscriber nodes. It starts a node with the name 'pi\_listener' which subscribes to the *order\_topic* topic. It reads messages of type *order\_msg* and calls the callback function upon completed data transfer. The callback function uses an if-else structure to check the order-field of the received variable, this is done to determine which Order-class variable should be updated. The variables sign and value fields are updated and the `.write()` function described in the Order class section above is called to send the data to the Nucleo.

```python
def callback(data):
	""" This function is called whenever there is the listener() receives a new message.
	    It first identifies which actuator the AI wants to write to and then updates the values and writes to the MCU. """
	    
	if data.order == 2:			# servo
        	servo.value = data.value
        	servo.sign = data.sign
        	servo.write()
		return
	else if ...
		.
		.
		.
	
```

`rospy.spin()` keeps python from exiting until the node has been terminated. Thus the node will stay subscribed to the topic and receive messages until the program is exited manually from the terminal. 


### Serial setup

```python
	ser_drive = serial.Serial("/dev/ttyACM1",115200)		
	ser_arm = serial.Serial("/dev/ttyACM0",115200)
	ser_drive.flushInput()					
	ser_arm.flushInput()                

	handshake()

	listener()					
	ser_drive.close()						
	ser_arm.close()
```

When the program starts the first thing that happens is that the serial ports to both Nucleos are opened with a baud rate of 115200 Bd/s. 
The baud rate sets how many bits of information are sent each second. 
A baud rate of 115200 Bd/s should be sufficient for the purposes of this project. 
One important detail is that the two units connected to each other use the same 
baud rate i.e, the Raspberry Pi and the Nucleo need to send and receive data at the same rate.

`.flushInput()` makes sure that there is no stray data in the buffer blocking the serial port. 

The `handshake()` function sends a message to both Nucleos and waits for the proper response before proceeding. 


```python

def handshake():

	msg_startup = bytearray()	
	msg_startup.append(hello)	
	msg_startup.append(hello)
	msg_startup.append(hello)
	ser_drive.write(msg_startup)
	ser_arm.write(msg_startup)

	while True:			
		response_drive = ser_drive.read(1)	
		response_arm = ser_arm.read(1)
		if response_arm == connected and response_arm == connected:
			print("Connection establised!")
			return
		else:
			print("MCU not connected")
```



## Demonstration of the communication protocol

The figure below shows how the data travels between all four nodes as an order to move the arm is issued by the AI. The two terminal windows to the left are opened on the computer and the ones to the right are opened on the Raspberry Pi using [SSH](https://en.wikipedia.org/wiki/Secure_Shell). 

<figure>
<img src = "pictures/terminator.png", width = 650>
<caption>Example of terminal view as the wheel loader is moving the arm.
</caption>
</figure>

The *Ai\_talker* node starts by sending a message containing \[order = 4, sign = 0, value = 255\]. 
This corresponds to moving the arm up with at maximum velocity. 
Shortly after a message containing \[order = 4, sign = 0, value = 0\] is sent to stop the arm. 
these two messages are sent over the *order_topic* topic and received by the *Pi_listener*.

When the actuator starts moving the change can be observed in the *Pi\_talker* window. 
This sensor data is sent over the *data\_topic* topic and received by the *Ai\_talker* node. 
As the arrows show, the data that is sent is the same as the one that is received. 




