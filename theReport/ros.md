# ROS - Robot Operating System

<What is ROS> 
Robot Operating System (ROS) is... 

## Nodes

The ROS framework consists of four nodes that communicate with each other as shown in the following diagram.

<figure>
<img src="pictures/ROS_nodes.png" width="650">
<figcaption>Layout of the ROS network showcasing active nodes and topics.<figcaption>
</figure>

There are two publisher nodes (AI\_talker and PI\_talker) and two subscriber nodes (AI\_listener and PI\_listener), one of each kind on the PC and the Raspberry Pi.
An example of a simple publisher and subscriber program in Python can be found [here](http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28python%29).

## Topics and messages

The nodes publish and subscribe to two different topics _/order_ and _/sensor_\__data_. 
The AI\_talker node publishes messages to the _/order_ topic with a frequency of 20 Hz, this frequency is used because it is the rate at which the AI sends commands as stated in the doctoral thesis (INSERT REF!).  
These messages are read by the PI\_listener node which then send them to the STM32 via the USB-port.
Then, when the raspberry pi receives sensor data from the STM32 the PI\_talker node publishes that data on the _/sensor_\__data_ topic for the AI_listener node to read.

The message type used in the program is defined in the order_msg.msg file and contains three unsigned 8-bit integers representing the order, the sign, and the value. 

<figure>
<img src="pictures/order_msg.png" width="250">
<figcaption>Order_msg.msg<figcaption>
</figure>

The final communication looks like this...

<figure>
<img src="pictures/communication_example.png" width="850">
<figcaptio>Example of how a command from the AI is processed and sent to the proper actuator, in this example the servo. <figcaption>
</figure>
