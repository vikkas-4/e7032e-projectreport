# Evolution of the Wheel Loader


Sins the start of the project both the appearance and functionality of the wheel loader has changed a lot. Some of the changes were due to the hardware component choices like the long motors we chose in order to save money, we taught that there would be no problems rebuilding the wheel loader in a small way in order to use them. We did not however expect the rippling effect that choice would have down the line that would remove crucial functionality of the loader like the midsection steering.


## Why is there no midsection steering?

The reason for removing the midsection hinge steams from the choice of motors. The motors length made the wheel loader extremely wide and unstable.


<figure>
<img src="pictures/img/beetle.jpg" width="550">
<figcaption>To wide and unstable version of wheel loader. <figcaption>
</figure>

In order to make the loader's width normal, the mounts for the motors had to be moved into the chassis where the mechanics for turning the midsections were located. Removing this is a problems sins a midsection hinge is a defining feature of wheel loaders. In our case, we decided it was an acceptable loss because our main focus was to go straight into a pile of gravel/sand and take a bucket sample. Steering the loader into position was not our main goal.


## Why removing the scissor frame?


At the same time as the motors for the wheels was inserted into the chassis, the mechanics for changing the height of the loader was lost. This ability is not part of an ordinary wheel loaders, hence no problem of losing it.


## Why removing the movable weight?

The movable counterweight is not part of a normal wheel loader and removing it had no effect on the project goals. This was also necessary in order to have a place to store the two nucleo computers and the raspberry pi.

<figure>
<img src="pictures/img/socket.jpg" width="550">
<figcaption>The movable weight on the back is now a computer
rack. <figcaption>
</figure>


## Why lengthening the bucket?


When inserting the wheel mounts into the chassis another problem occurred. The wheels had to be installed on a lower plane for them to fit the new design, this mad the chassis raise up from the ground a considerable length that mad the bucket no longer being able to touch the ground in its lowest state. Some redesigning was possible to get this distance shorter. But in the end, the only way for getting the bucket completely down on ground level was to lengthen it.


This extra length will require extra power for the motors to lift the same weight and sins lifting the bucket is what requires the most power from any of the motors, this could have been a severe problem. Luckily we had chosen motors that were powerful enough even with the added moment arm length.


<figure>
<img src="pictures/img/highscoop.jpg" width="550">
<figcaption>Broken design of bucket in lowest possible positing. <figcaption>
</figure>

Here the scoop is lengthened a small distance after the redesign of the front end of the chassis.

<figure>
<img src="pictures/img/BucketFixed.jpg" width="550">
<figcaption>Fixed lengthened bucket in lowest possible positing. <figcaption>
</figure>
