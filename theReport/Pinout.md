# Connecting the electronics

Now that all the electronics are chosen they need to be connected. We start by looking at what components need to be connected to the Nucleos/Raspberry Pi and what type of connections are needed. This information can be seen in the table below. 

| Component | Pin type | # of pins |
|--------|:--------|:--|
| Encoder | Interrupt | 8 |
| Motor controller | PWM | 6 |
| Motor controller, Standby | Digital | 2 |
| Motor controller, Mode | Digital | 6 |
| Servo | PWM | 1 |
| Current sensor | I2C | 2 |

## Interrupts

\noindent Eight interrupt pins are needed for the encoders. The GPIO pins on the Raspberry Pi can be configured to handle interrupts, but since it is not a real-time system there might be problems with missing interrupts. Because of this we instead use the Nucleos to handle the interrupts.

A Nucleo board has 16 external interrupt lines that are connected to seven interrupt handlers. The 16 interrupt lines are each connected to more than one pin. Since it was not clear how these pins where connected at the time of allocation, one of these pins are used for the interrupt and the rest are left unused. The first five interrupt lines are connected to one interrupt handler each, the next five lines are connected to one handler and the last six lines are connected to one handler.

We use four interrupt pins on each Nucleo board, where one board is used for the arm and bucket motors and the other is used for the drive motors. We want to use four interrupt handlers on each board to make the code faster. We use the handlers 0, 1, 4 and 5-9 on both boards. The reason we are not using handlers 2 and 3 is that they are used for USART communication. 

## PWM

Seven PWM signals are needed in total, one for the servo and one for each motor. The PWM signal sent to the servo determines the position of the servo and the PWM signals used for the motors are sent to the motor controller to set the speed of the motors.

The Nucleo boards are used to generate the PWM signals since they are better at doing this than the Raspberry Pi. The Nucleo board we are using has eight different timers that can be used to create the PWM signals. Seven of the timers are general-purpose timers (with some different specifications) and one is an advanced-control timer (TIM1). The advanced-control timer has four independent channels and is used to generate the PWM signals used for the motors since it has full modulation capability (duty cycle from 0 to 100\%). One of the general-purpose timers (TIM3) is used to generate the PWM signal for the servo. The reason why this is used is mainly due to convenience when allocating all of the pins since all of the general-purpose timers could have been used. The PWM signals for the servo and drive motors are generated on one Nucleo board and the PWM signals for the arm and bucket motors are generated on the other. 

## Digital

Eight digital pins are needed in total, all for the motor controllers. Two of the pins are used for the standby function and six of the pins are used for mode control. The standby function is used to turn the output from the motor controllers on and off (essentially turning the motors on and off) and the mode control is used to set the rotational direction of the motors.

The Nucleo boards are used for these digital signals since they are working closely together with the PWM signals. The Nucleo boards are each responsible for one of the standby signals. The motor controllers connected to the Nucleos are taken out of standby as soon as the Nucleos are connected correctly to the Raspberry Pi and ready for use. Four of the mode signals are taken from one Nucleo board and sent to one motor controller. Two of these signals are used for the rotational direction of the arm and two for the bucket. The last two mode signals are taken from the other Nucleo board and sent to two of the motor controllers. They are used for the rotational direction of the drive motors. We use the same signals to all drive motors since we always want them to drive the wheel loader in the same direction at the same time. 

## I2C

Two pins are needed for I2C communication with the current sensors. They are two special pins that are called SCL (clock line) and SDA (data line) and they are used to connect both current sensors in parallel. These pins exist both on the Raspberry Pi and the Nucleos and we will connect them to the Raspberry Pi since this data is only needed for the AI. When connecting them to the Raspberry Pi we will not have to send this data from the Nucleo and thus freeing up some bandwidth and decreasing delay. 

## Pinout tables

Now that we have figured out how everything needs to be connected we can create pinout tables for the Nucleos and Raspberry Pi. The Nucleo has names on the pins that are used when programming the board. They look like PXX(X) where the first X is a letter and the rest are numbers. All of these can be used as digital pins and for interrupts, but no two pins with the same numbers can be used as interrupts at the same time. Some of them can be used for generating PWM signals and those are also denoted PWMn/c, where n is the timer number and c is the channel. Below all three tables can be seen. 

### Nucleo for wheels and servo

| Pin number | Description | Usage | Comment |
|--------|:--------|:--|:--|
| CN7 |  |  |  |
| 1 | PC10 |  |  |
| 2 | PC11 |  |  |
| 3 | PC12 |  |  |
| 4 | PD2 |  |  |
| 5 | VDD, 3V3 |  |  |
| 6 | E5V |  |  |
| 7 | BOOT0 |  | Don't use |
| 8 | GND |  |  |
| 9 | NC |  |  |
| 10 | NC |  |  |
| 11 | NC |  |  |
| 12 | IOREF |  |  |
| 13 | PA13 |  | Don't use |
| 14 | RESET |  |  |
| 15 | PA14 |  | Don't use |
| 16 | 3V3 |  |  |
| 17 | PA15, PWM2/1 |  |  |
| 18 | 5V |  |  |
| 19 | GND |  |  |
| 20 | GND |  |  |
| 21 | PB7, PWM4/2 |  |  |
| 22 | GND |  |  |
| 23 | PC13 |  | User button |
| 24 | VIN |  |  |
| 25 | PC14 |  |  |
| 26 | NC |  |  |
| 27 | PC15 |  |  |
| 28 | PA0, PWM2/1 | Encoder FR Wheel |  |
| 29 | PH0 |  |  |
| 30 | PA1, PWM2/2 | Encoder RR Wheel|  |
| 31 | PH1 |  |  |
| 32 | PA4 | Encoder RL Wheel|  |
| 33 | VBAT |  |  |
| 34 | PB0 |  |  |
| 35 | PC2 |  |  |
| 36 | PC1 |  |  |
| 37 | PC3 |  |  |
| 38 | PC0 |  |  |
| CN10 |  |  |  |
| 1 | PC9, PWM3/4 |  |  |
| 2 | PC8, PWM3/3 |  |  |
| 3 | PB8, PWM4/3 | Motor controller, Mode, A/BIN1 | Wheel motors |
| 4 | PC6, PWM3/1 | PWM Servo |  |
| 5 | PB9, PWM4/4 | Motor controller, Mode, A/BIN2 | Wheel motors |
| 6 | PC5 |  |  |
| 7 | AVDD |  |  |
| 8 | U5V |  | 5V from USB |
| 9 | GND |  |  |
| 10 | NC |  |  |
| 11 | PA5, PWM2/1 | Encoder FL Wheel|  |
| 12 | PA12 |  |  |
| 13 | PA6, PWM3/1 |  |  |
| 14 | PA11, PWM1/4 | PWMA Motor controller #1 | FR Wheel |
| 15 | PA7, PWM1/1N |  |  |
| 16 | PB12 |  |  |
| 17 | PB6, PWM4/1 |  |  |
| 18 | NC |  |  |
| 19 | PC7, PWM3/2 |  |  |
| 20 | GND |  |  |
| 21 | PA9, PWM1/2 | PWMB Motor controller #1 | FL Wheel |
| 22 | PB2 |  |  |
| 23 | PA8, PWM1/1 | PWMA Motor controller #2 | RR Wheel |
| 24 | PB1, PWM1/3N |  |  |
| 25 | PB10, PWM2/3 | Motor controller, Standby |  |
| 26 | PB15, PWM1/3N |  |  |
| 27 | PB4, PWM3/1 |  |  |
| 28 | PB14, PWM1/2N |  |  |
| 29 | PB5, PWM3/2 |  |  |
| 30 | PB13, PWM1/1N |  |  |
| 31 | PB3, PWM2/2 |  |  |
| 32 | AGND |  |  |
| 33 | PA10, PWM1/3 | PWMB Motor controller #2 | RL Wheel |
| 34 | PC4 |  |  |
| 35 | PA2, PWM2/3 | Used for USB comunication |  |
| 36 | NC |  |  |
| 37 | PA3, PWM2/4 | Used for USB comunication |  |
| 38 | NC |  |  |

### Nucleo for arm and bucket

| Pin number | Description | Usage | Comment |
|--------|:--------|:--|:--|
| CN7 |  |  |  |
| 1 | PC10 |  |  |
| 2 | PC11 |  |  |
| 3 | PC12 |  |  |
| 4 | PD2 |  |  |
| 5 | VDD, 3V3 |  |  |
| 6 | E5V |  |  |
| 7 | BOOT0 |  | Don't use |
| 8 | GND |  |  |
| 9 | NC |  |  |
| 10 | NC |  |  |
| 11 | NC |  |  |
| 12 | IOREF |  |  |
| 13 | PA13 |  | Don't use |
| 14 | RESET |  |  |
| 15 | PA14 |  | Don't use |
| 16 | 3V3 |  |  |
| 17 | PA15, PWM2/1 |  |  |
| 18 | 5V |  |  |
| 19 | GND |  |  |
| 20 | GND |  |  |
| 21 | PB7, PWM4/2 |  |  |
| 22 | GND |  |  |
| 23 | PC13 |  | User button |
| 24 | VIN |  |  |
| 25 | PC14 |  |  |
| 26 | NC |  |  |
| 27 | PC15 |  |  |
| 28 | PA0, PWM2/1 | Encoder Arm A|  |
| 29 | PH0 |  |  |
| 30 | PA1, PWM2/2 | Encoder Arm B|  |
| 31 | PH1 |  |  |
| 32 | PA4 | Encoder Bucket A|  |
| 33 | VBAT |  |  |
| 34 | PB0 |  |  |
| 35 | PC2 |  |  |
| 36 | PC1 |  |  |
| 37 | PC3 |  |  |
| 38 | PC0 |  |  |
| CN10 |  |  |  |
| 1 | PC9, PWM3/4 |  |  |
| 2 | PC8, PWM3/3 |  |  |
| 3 | PB8, PWM4/3 | Motor controller, Mode, AIN1 | Bucket motor |
| 4 | PC6, PWM3/1 |  |  |
| 5 | PB9, PWM4/4 | Motor controller, Mode, AIN2 | Bucket motor |
| 6 | PC5 |  |  |
| 7 | AVDD |  |  |
| 8 | U5V |  | 5V from USB |
| 9 | GND |  |  |
| 10 | NC |  |  |
| 11 | PA5, PWM2/1 | Encoder Bucket B|  |
| 12 | PA12 |  |  |
| 13 | PA6, PWM3/1 |  |  |
| 14 | PA11, PWM1/4 | PWMA Motor controller | Bucket Motor |
| 15 | PA7, PWM1/1N |  |  |
| 16 | PB12 |  |  |
| 17 | PB6, PWM4/1 | Motor controller, Mode, BIN1 | Arm motor |
| 18 | NC |  |  |
| 19 | PC7, PWM3/2 | Motor controller, Mode, BIN2 | Arm motor |
| 20 | GND |  |  |
| 21 | PA9, PWM1/2 |  |  |
| 22 | PB2 |  |  |
| 23 | PA8, PWM1/1 |  |  |
| 24 | PB1, PWM1/3N |  |  |
| 25 | PB10, PWM2/3 | Motor controller, Standby |  |
| 26 | PB15, PWM1/3N |  |  |
| 27 | PB4, PWM3/1 |  |  |
| 28 | PB14, PWM1/2N |  |  |
| 29 | PB5, PWM3/2 |  |  |
| 30 | PB13, PWM1/1N |  |  |
| 31 | PB3, PWM2/2 |  |  |
| 32 | AGND |  |  |
| 33 | PA10, PWM1/3 | PWMB Motor controller | Arm motor |
| 34 | PC4 |  |  |
| 35 | PA2, PWM2/3 | Used for USB comunication |  |
| 36 | NC |  |  |
| 37 | PA3, PWM2/4 | Used for USB comunication |  |
| 38 | NC |  |  |

### Raspberry Pi

| Pin number | Description | Usage |
|--------|:--------|:--|
| 1 | 3v3 |  |
| 2 | 5v |  |
| 3 | GPIO 2, I2C(SDA1), I2C(SDA3), |  |
| 4 | 5v |  |
| 5 | GPIO 3, I2C(SCL3), |  |
| 6 | GND |  |
| 7 | GPIO 4, I2C(SDA3), |  |
| 8 | GPIO 14|  |
| 9 | GND |  |
| 10 | GPIO 15|  |
| 11 | GPIO 17|  |
| 12 | GPIO 18|  |
| 13 | GPIO 27|  |
| 14 | GND |  |
| 15 | GPIO 22, I2C(SDA6), |  |
| 16 | GPIO 23, I2C(SCL6), |  |
| 17 | 3v3 |  |
| 18 | GPIO 24|  |
| 19 | GPIO 10, I2C(SDA5), |  |
| 20 | GND |  |
| 21 | GPIO 9, I2C(SCL4), |  |
| 22 | GPIO 25|  |
| 23 | GPIO 11, I2C(SCL5)|  |
| 24 | GPIO 8, I2C(SDA4), |  |
| 25 | GND |  |
| 26 | GPIO 7, I2C(SCL4), |  |
| 27 | GPIO 0, I2C(SDA0), | Current sensor |
| 28 | GPIO 1, I2C(SCL0)| Current sensor |
| 29 | GPIO 5, I2C(SCL3), |  |
| 30 | GND |  |
| 31 | GPIO 6, I2C(SDA4), |  |
| 32 | GPIO 12, I2C(SDA5),|  |
| 33 | GPIO 13, I2C(SCL5), |  |
| 34 | GND |  |
| 35 | GPIO 19|  |
| 36 | GPIO 16|  |
| 37 | GPIO 26|  |
| 38 | GPIO 20|  |
| 39 | GND |  |
| 40 | GPIO 21|  |

## Schematics

To make it easier to connect all the cables some simple schematics for all of the motors where drawn as seen below. 

<figure><img src="pictures/schematicFLwheel.png" width="450"><figcaption>Schematic for the front left wheel motor <figcaption></figure>

<figure><img src="pictures/schematicFRwheel.png" width="450"><figcaption>Schematic for the front right wheel motor <figcaption></figure>

<figure><img src="pictures/schematicRLwheel.png" width="450"><figcaption>Schematic for the rear left wheel motor <figcaption></figure>

<figure><img src="pictures/schematicRRwheel.png" width="450"><figcaption>Schematic for the rear right wheel motor <figcaption></figure>

<figure><img src="pictures/schematicArm.png" width="450"><figcaption>Schematic for the arm motor <figcaption></figure>

<figure><img src="pictures/schematicBucket.png" width="450"><figcaption>Schematic for the bucket motor <figcaption></figure>