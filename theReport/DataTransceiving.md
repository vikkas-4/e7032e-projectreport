# Data Transceiving

## STM32 Receiving Data from UART

The STM32 microcontrollers receive data through UART. This communication is setup in CubeMX and then connected to the RasPI via USB.

To receive data the following code is implemented inside the loop:

```c
// Get data from UART and save it to "indata"
HAL_UART_Receive_IT(&huart2, indata, 3);
```

The function takes the handle for UART2 and stores a 3bit data to the variable indata.

## STM32 Transmitting Data through UART

Transmitting back data to the RasPI is as simple as receiving. use the same handle for UART2 but now sending the variable which is to be sent and specify both size and also a delay where the data can be sent, which in this example below is 10ms.

```c
// Send sensor data
HAL_UART_Transmit(&huart2, driveSensorData, 3, 10);
```

The delay is so that the transmission is completed with a greater stability.