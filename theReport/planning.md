# Planning the Project

Since projects are carried out over an extended period of time, planning is an essential part of every project. Sometimes planning is the most difficult part of the project since most projects are highly ambiguous in the beginning and the goal of planning the project is to make a clear path on how this ambiguity can be sorted out.

## WBS and Gantt

Some planning tool such as a WBS and Gantt chart is developed to structure what and when tasks should be carried out. This is in order to be sure that the tasks are completed within the scope of the project but also that dependent tasks are completed in sequence. This means that if a task is dependent on another before it can be started the dependent task needs to be completed before the other.

## Communication

Sometimes a Gantt and WBS can be difficult to follow so instead [Trello](https://trello.com/) was used to do weekly planning for the group to ensure that everyone knew what to do at all times.

[Telegram](%5Bhttps://telegram.org/%5D(https://telegram.org/) also proved to be a valuable resource in the project for communicating things in the project. Primarily used for discussions and planning meetings.

Meetings was performed in the dedicated project room if necessary.

## Workflow

Defining a workflow ensures that work is performed in the correct way in accordance with the other members of the project.

### Group workflow

Using the different resources of planning and communicating through the project the group workflow can be defined. Using WBS and Gantt as the overall long-term planning tool for the project, Trello as the weekly planning tool, Telegram for communication and meetings when necessary.

But the most important part of the group workflow is planning tasks so that different people are not working on the same thing at the same time. This improves the efficiency in the project and save time in merge parts together. This is done by the Trello and communicated in Telegram by the project manager.

### Git Workflow

Git is a very valuable tool to use for version control and managing files in a project but it comes at a cost of ambiguity in how everyone should work. This issue is addressed and solved with the framework, workflow, shown below.

<figure>
<img src = "pictures/Presentations_Pictures_gitWorklfow.png" width = "650">
<caption>Git workflow.
</caption>
</figure>

The pull requesting and two person approval feature is established in order to force project members to revise each others work, making them accountable for the changes they made. Using this workflow ensures that everything is up to date and files are correctly updated in the project.
