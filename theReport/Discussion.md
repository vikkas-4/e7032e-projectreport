# Discussion

## Did the project follow the planning?

The short answer to this question is no. Although the project was planed for implementing additional functionality such as measuring the current draw from the motors to enable estimation of the lift force the time scope of the project was not enough. 

When working with the project we encounter some troubles with ordering the components. We wanted to order components that was not in the list of supported companies. This took up time progressing in the project because we had to fins the equivalent parts on the supported webpages. This could have been avoided if the sources we should have ordered from was more clear from the beginning.

Also when working with the project several pieces o the project revealed themselves taking much longer than expected as we worked with them. This is because of our lack of experience working in projects such as this one and working in projects at all is new to us. But throughout the course we have learned so much valuable information on how projects should be conducted and worked with.

## Are we Satisfied with the Project? Was it successful?

Yes! We have had so much fun working with the Lego wheel loader and learnt so much about programming, electronics, git and project management. The primary lesson we will take with us in the future is how you should work within a team. Everyone should be heard and every idea should be considered. Everyone should feel that the are doing something meaningful and contributing to the project while also having fun. We feel like we have achieved this and that is why we consider this project as a success.

## Future of the Project

Since there are several things that can be improved and implemented there is still a lot of work that could be done for future teams. If the next years students in the project course would want to work with this project, here is some stuff that could be done/improved:

* More reliable communication
* Control action for more satisfactory movement of arm/scoop and drive
* Implementation of a autonomous AI/machine learning algorithm using the actuators and sensors available

There is probably a millions thing that could be done with a project like this but the most important thing is to have fun doing it!