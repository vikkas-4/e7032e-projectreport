# Software and overall code structure

<!-- Software part of the INA219 current sensor is included in the "current sensor" section. Maybe parts of it or all of it can be removed to this section. Or maybe we could just refer to the "current sensor" section of it.-->

## Python 
<why python?>Python is the programming language of choice for writing code for the Raspberry Pi and the PC. It is possible to program ROS in Python, Lisp, and C++. Python is preferred since it is a higher-level language which overall makes it easier to program in as well as it being a widely used language means that there is a lot of documentation online. <ROS melodic that we are using in the Raspberry-Pi is compatible with python 2.7 and is not compatible with python 3. Therefore even the code for bringing data from the current sensor is python 2.7 code using libraries for SMBus (SMBus is very similar to i2c). >

### Required packages:
#### _pySerial_
PySerial is a package that facilitates communication via the serial port. Functions such as `serial.write()` and `serial.read()` are essential for accessing the serial port. The pySerial API can be found [here](https://pythonhosted.org/pyserial/pyserial_api.html).

#### _rospy_
The rospy package makes it possible to program ROS in python. The package provides functions to set up nodes, send and receive data from topics and more. The main purpose of this package is so that the data sent over the ROS Topics is able to be handled and used by the different programs shown in the next section. Documentation for the rospy package can be found on the [ROS-wiki](http://wiki.ros.org/rospy). 

#### _pygame_
The pygame package is used to get inputs from a hand controller and import them into python. The controller will be used as a stand-in for the AI, simulating the control signals. Pygame documentation can be found [here](https://www.pygame.org/docs/).

### Directory structure:
<explain the difference between modules and packages>A module is a piece of software with a specific functionality, any file with the _.py_ extension can be considered a module. Functions, variables, classes, etc. can easily be imported and used in other modules with the `import` command. The directory structure on the PC and the Raspberry Pi is shown below.
<figure><img src="pictures/Directory_structure .png" width="550"><figcaption>Directory structure on the PC. <figcaption></figure>
<figure><img src="pictures/Directory_structure (pi).png" width="550"><figcaption>Directory structure on the Raspberry Pi. <figcaption></figure>

#### aidriver & ros_node
The system uses two ROS packages, _aidriver_ on the PC and _ros_\__node_ on the Raspberry Pi. Packages are the main way of organizing ROS software. A package can contain nodes, messages, services, topics, third-party software, etc. 

#### msg
Folder containing message types used within the package. The message type is defined in a _.msg_-file. 

#### scripts
Folder containing the nodes used by the package. The nodes are initialized in python scripts. 

#### package.xml
This file is required in any catkin package and contains basic information about the package such as package name, authors, version numbers, and dependencies on other catkin packages. This is mainly of concern when releasing a package to the ROS community since this package is not meant to be released the contents of this file are not important. 

#### CMakeLists.txt
This file is also a required part of the package and it tells the compiler how to build the package as well as what other dependencies needs to be installed. 

 
### Communication protocol 
<explain orders>To communicate between the Raspberry Pi and the STM32 data needs to be sent to the STM32 which then interprets it and sets the proper actuators accordingly. In order to know which value is meant for which actuator each value is sent with a reference byte. This reference byte is referred to as an "order". 
Each message sent between the Raspberry Pi and the STM32 contains three parts, one byte each<Subject to change!!!>.The first byte refers to one of several predefined orders stated in the following table. Though only four bits are required to represent all possible orders, one byte is sent in order to simplify the code and avoid handling individual bits.


| Order | Reference(byte) | Reference(bits) |
| ------------ | --------------- | --------------- |
| hello | 0 | 0000 |
| connected | 1 | 0001 |
| servo | 2 | 0010 |
| drive | 3 | 0011 |
| arm_speed | 4 | 0100 |
| bucket_speed | 5 | 0101 |
| arm_pos | 6 | 0110 |
| bucket_pos | 7 | 0111 |
| arm_current | 8 | 1000 |
| bucket_current | 9 | 1001 |
| stop | 10 | 1010 |


The "hello" and "connected" orders are only used once on startup to ensure that the connection between the Raspberry Pi and the STM32 is established. Orders 2 through 5 are used to represent the different actuators. Orders 6 through 9 are only used for receiving sensor data from the STM32 and the current sensors. The "stop" order immediately shuts off all actuators. This order can be sent from the AI over ROS or initialized locally on the Raspberry Pi if it detects an error.<should we stop completely if connection is lost??> 

The second byte is used to set the direction of the actuator. For positive values i.e the actuator rotating forward, this byte is set to zero. Respectively, one represents a negative reference value. This method is used to avoid having to use signed integers since these would be harder to send and interpret correctly on the STM32. 
The last byte contains an unsigned integer value in the range [0,255]. With one byte this is the range of numbers that can be represented. Since the plastic parts are rather flimsy there will be a not-insignificant amount of hysteresis in the system. This means that a higher resolution will not necessarily increase the precision of the actuators. Combined with the fact that the system does not require super high precision in order to complete its task a resolution of one byte i.e 256 levels was deemed sufficient.