# Arm and Bucket

This section describes how the arm and Bucket is controlled using the STM32

## Arm

The arm is controlled by a DC motor much like the drive. The value is received by the USART is converted from a range of 0-1023 to the desired range 0-XX depending on what the maximum speed we want to set the movement to. Is the code example below the max is set to 30. This is because no safety is implemented in the extremums of the position of the arm so I can rotate more than it is possible.

More information on how everything works can be found in the [drive](drive.md) section.

Up and down is controlled by setting the desired pins HIGH or LOW when the value received is above or below the center of 512 as shown is the example below.

```c
if (armValue > 520)
{
    // Up direction
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_SET);

    // Set the speed
    htim1.Instance->CCR3 = map(armValue, 512, 1024, 0, 30); // Set duty cycle
}
else if (armValue < 500)
{
    // Down direction
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);
    // Set the speed
    htim1.Instance->CCR3 = map(armValue, 0, 512, 30, 0); // Set duty cycle
}
else
{
    // Stop
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, GPIO_PIN_RESET);

    // Set the speed
    htim1.Instance->CCR3 = 0;
}
```

## Bucket

The bucket is controlled in the same way as the arm. See arm section for more information. The code is presented below.

```c
if (bucketValue > 520)
{
    // Up direction
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);

    // Set the speed
    htim1.Instance->CCR4 = map(bucketValue, 512, 1024, 0, 30); // Set duty cycle
}
else if (bucketValue < 500)
{
    // Down direction
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);

    // Set the speed
	htim1.Instance->CCR4 = map(bucketValue, 0, 512, 30, 0); // Set duty cycle
}
else
{
    // Stop
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_RESET);

    // Set the speed
    htim1.Instance->CCR4 = 0;
}
```

