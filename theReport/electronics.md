# Electronics

This section describes the functions of every electronic component used in this project.

## Current sensor
In order to know the weight of the load the wheelloader is scooping, a way of measuring the weight is needed. The way that is used in the doctoral thesis <!-- källa här --> is by measuring pressure variations in the hydraulic piston in the arm. In the case of this project, because of the size restrictions of the LEGO-wheelloader, after some research the conclusion that it is inconvinient to try to use a pressure sensor in the same way as used in the full sized wheelloader in the doctoral thesis. Here, motors are used instead of hydralic pistons and therefore two current sensors are used here, where one of them measures the current consumed by the arm motor and one for the bucket. The current sensors are small chips called INA219 from Texas Instruments and are assembled on PCB bords by Adafruit. 

The INA219 is a high side current sensor, which means it is connected in serie between the current source and the load. It can then output four parameters: shunt voltage, bus voltage, current and power. In this project, the interesting one of this four outputs is the shunt voltage. <!-- källa (INA219 datasheets) -->

<figure>
<img src="pictures/DC-Current-Sensor.png" width="250">
<figcaption>The INA219 (by Texas Instruments) mounted on a PCB (by Adafruit). <figcaption>
</figure>



Shunt voltage is voltage over a shunt resistor and a simplified explination of a shunt resistor is that it is a resistor with resistance that is low inought that it barely affects the system (circuit) and high enough that it can be measured. So according to Ohms law, V = RI, with a known resistance and measuring voltage over it, the current flowiing through it can be figured out. <!-- Chapter "8 Detailed Desctiption" in the TI-datasheet for INA219 -->

<!-- Considerations: -->
In this project, the current sensors are not going to be used so that they can reach temperature that is so high that their performance gets affected. The same thing with frequency, input current, I<sup>2</sup>C clock frequency. <!-- Section "7.7 Typical Characterstics" in the TI-datasheet for INA219 -->

[Datasheet of the current sensor (Adafuit)](theReport/Current-Sensor/Current-Sensor-Datasheets/adafruit-ina219-current-sensor-breakout.pdf)

[Datasheet of the current sensor (Texas Instruments)](theReport/Current-Sensor/Current-Sensor-Datasheets/ina219.pdf)



## Motor

The motors used in the project are the FIT0521-geared motor with encoder built into the frame.
The motor is a regular geared DC-motor with a gear reduction of 1:34 and a no-load max speed of 210RPM. The motor is driven by a 6V DC voltage and has a maximum stall torque of 10kgcm at 3.2A.


<figure>
<img src="pictures/img/FIT0521-45.jpg" width="250">
<figcaption>FIT0521-geared motor with encoder. <figcaption>
</figure>

## Motor Controller

To control the DC-motor the motor controller ROB-14451 is used. The board can handle up to 15V DC motor voltage which is well above the 6V necessary for the FIT0521 motor. The ROB-14451 can handle current draw on average of 1.2A per motor and peaks up to 3.2A per motor.  
The logic of the board are powered by 5V and uses a TB6612FNG H-bridge IC to supply the correct voltage and current to the motor.  
The pinout and explanation of the pins can be found in the datasheet [here](links/ROB-14451.pdf).



<figure>
<img src="pictures/motorController.png" width="250">
<figcaption>Motor Controller.<figcaption>
</figure>


#### H-bridge

To control the motors an H-bridge is used. The switches S1-S4 are controlled by the inputs A/B IN1/2 on the controller board and are used to determine the direction of rotation of the motor M by changing the direction the current flows through the motor.  
By choosing the Voltage in V_in as a pwm signal the speed can be controlled by changing the duty cycle of the pwm signal.  
The logic 5V pwm signal input is converted to the full 6V pwm signal that goes to the motor keeping the amplitude at the voltage which the motor works best at. This way power is saved and the motor can be controlled at much slower speeds without stalling since the motor always is at full strength.


<figure>
<img src="pictures/H_bridge.png" width="250">
<figcaption>H-bridge<figcaption>
</figure>


## Encoder

The FIT0521-geared motor used in this project has a quadrature encoder, this is a type of incremental encoder used for sensing direction and movement.

Apart from direction and movement, position can also be monitored with a quadrature encoder by producing another signal known as the “Z channel”. This Z signal is produced once per complete revolution of the encoder and is often used to locate a specific position during a 360° revolution of the motor.

A quadrature encoder normally has at least two outputs, Channel A and B, each of which will produce digital pulses when the motor is in motion. These pulses will follow a particular pattern that allows you to tell which direction the thing is moving, and by measuring the time between pulses, or the number of pulses per second, you can also derive the speed.


<figure>
<img src="pictures/encoder/quadencoder.gif" width="450">
<figcaption>Animation showings what happens inside the encoder.<figcaption>
</figure>





They have a black and white reflective code wheel inside the wheel rim, and the PCB has a pair of reflective sensors that point at the code wheel. These sensors have a spacing between them relative to the stripes on the wheel to produce the pattern of pulses in the animation.

These pulses are 90 degrees out of phase meaning that one pulse always leads the other pulse by one quarter of a complete cycle, a cycle is a complete transition from low -> high -> low again.

The order in which these pulses occur will change when the direction of rotation changes. The two diagrams below show what the two pulse patterns look like for clockwise and counterclockwise rotation.

<figure>
<img src="pictures/img/quaddiag.png" width="450">
<figcaption>Pulse train demonstrating rotation direction of motor.<figcaption>
</figure>



### Channel A at the top - clockwise rotation:

When Channel A goes from LOW to HIGH
and Channel B is LOW.

### Channel A - counterclockwise :

When channel A goes from LOW to HIGH
and channel B is HIGH.



## Servo

To steer the wheel loader a Tower Pro MG995R servo is used. It is a high torque standard size servo (40.7 x 19.7 x 42.9mm, 62.41g) with metal gears. It has an input voltage of 4.8-6V, has a torque of 8.5-10 kg-cm and a speed of 0.20-0.16 sec/60&deg;. 

<figure>
    <img src="pictures/servo2.jpg" width="350">
    <figcaption>Tower Pro MG995R</figcaption> 
</figure>

## STM32 - Nucleo

The microcontoller of choice is the STM32 - Nucleo board.
The Nucleo is used because it has a large amount of analog pins, in particular it comes with six interrupt pins which are necessary for reading the pulses sent from the encoders.

<figure>
    <img src="pictures/stm32.jpg" width="350">
    <figcaption>STM32.</figcaption> 
</figure>


## Raspberry Pi

The Raspberry Pi 4 is used to enable network communication between the AI and the wheel loader. It comes with a built in wifi-receiver and can also set up its own network and let other units connect to it directly without the need for an external router. 

<figure>
    <img src="pictures/RasPi.jpg" width="350">
    <figcaption>Raspberry Pi 4 Model B</figcaption> 
</figure>


