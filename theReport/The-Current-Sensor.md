#
For the user (an AI or a preson) controlling the wheel loader to see how much weight that is being scooped, a current sensor is used to measure the current draw due to the actuators on the bucket and bucket-arm. This communication would be done according the use-diagram below:

<figure>
<img src="pictures/Use-diagrams/use-diagram-01.png">
<figcaption>Simplified use-diagram of the wheel loader.<figcaption>
</figure>

--- ---

To creat a master code for [the i2c communication](theReport/i2c-protocol/i2c-protocol-master.md) between the micro controller (Arduina/STM-32) and the two current sensors (INA219). The micro controller would be the master and the current sensors would be slaves. The INA219 pre programmed with slave cod for the i2c protocol. The defauld i2c adress of the sensor would be changed by shorting two points on the circuit board. 

This information would then be sent to a ROS-node which would be located in the Raspberry-pi which then would send it to the ROS-node that is in the desktop that sends data to the wheeloader. The same information would then be sent to the person or AI that needs to know the weight of the load that's being lifted. Some where between the current sensors and the preson, the current draw value (unit: Amper) would be translated into kilograms or grams.  

<figure>
<img src="pictures/Use-diagrams/use-diagram-02.png">
<figcaption>Data from the current sensor sent to the MCU.<figcaption>
</figure>

This was concidered as complicating things more than necessary so a different approach was needed. But a Raspbian Lite operating system that contains ROS-kinetic and Circuit Python and some additional softwares is available as an iso image and is ready to be used at any.

--- ---

Later the plan changed to sending the current sensor data to the Raspberry-Pi (using Circuit Python libraries to import data through the i2c protocol) and then send the data to the ROS within the Raspberry-pi which transmits with the ROS on the server and then display weight of the load to the user in grams or kilograms. <!-- (see fig. ) -->

<figure>
<img src="pictures/Use-diagrams/use-diagram-04.png">
<figcaption>Data from the current sensor sent to the Raspberry-Pi (through Circuit Python libraries).<figcaption>
</figure>


This would be a not so bad solution. But when it was time to impliment the plan, a problem was encountered immediately: the Python Circuit uses python 3 and the ROS distro that is used here - ROS Kinetic -  uses python 2.7. Perhaps there is a way of getting oround this problem (no idea what that wuld be though) and stik to this plan but to avoid complications later on during this project, a simpler method was desired which led to the third plan.

--- ---

The third plan: The same as the previous but without Circuit Python. This means using ROS's own i2c interface to recieve the data from the current sensor <!-- (see fig. ) -->. After som research on ROS, the discovery that ROS has no hardware interface such us i2c. Therefore something else should handle this part. Which led to the fourth plan.

<figure>
<img src="pictures/Use-diagrams/use-diagram-06.png">
<figcaption>Data from the current sensor sent to the Raspberry-Pi (Using some "ROS hardware interface").<figcaption>
</figure>

--- ---

The fourth plan: To fall back to using python to import the i2c data. But this time using python 2.7. Well it was possible to import data via the I2C-protocol from the current sensor into the Raspberry-Pi using python 2.7. But the output was hard to understand. This was due to lack of enough knowledge in how sensors such us the ina219 work. I2C address of the device and the register from which data gets picked. The INA219 can generate four parameters as outputs; shunt voltage, bus voltage, current, and power sensed. It was hard to know wich address was for the shubt voltage because more than one address reacted on the current input but the values generated were maximum value if no input and minimum if there is an input, for all values of input. 

How the I2C works was by this time understood but how to write a python code that for the master to read data from a slave was not so trivial. It was hard to find python 2.7 libraries for I2C so next thing to try was checking out the SMBus protocol. The current sensor INA219 is compatibel with both I2C and SMBus. But SMBus is very simmilar to I2C. It only has three wires - SCL, SDA and GND - and the rest of simmilarities include what actualy is need from I2C. So after not much research, a change of plan was made.

--- ---

Appendix of this part of the report (klickable links)

[The I2C Protocol](theReport/i2c-protocol/i2c-protocol-master.md)

[Datasheet of the current sensor (Adafuit)](theReport/Current-Sensor-Datasheets/adafruit-ina219-current-sensor-breakout.pdf)

[Datasheet of the current sensor (Texas Instruments)](theReport/Current-Sensor-Datasheets/ina219.pdf)


<!-- -->
<!-- -->

