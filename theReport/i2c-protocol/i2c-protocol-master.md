
# The i<sup>2</sup>c protocol seen from a masters point of view

## Steps of the Master-Slave i2c communication

### Master sending data to a slave:
<pre>
1)			Master:	Start (1 bit)

2)			Master:	Adress of a slave (7/10 bits)

3)			Master:	R/W (sending) (1 bit)
4)			Slave:	Send ACK/NACK (1 bit)

5)			Master decides to send data to a slave

  5.1.1)		Master:	Data frame 1 (8 bits)
  5.1.2)		Slave:	Send ACK/NACK (1 bit)
  
  5.2.1)		Master:	Data frame 2 (8 bits)
  5.2.2)		Slave:	Send ACK/NACK (1 bit)
  
  5.n.1)		Master:	Data frame n (8 bits)
  5.n.2)		Slave:	Send ACK/NACK (1 bit)

6)			Master:	Stop (1 bit)
</pre>

---

### Master receiving data from a slav:
<pre>
1)			Master:	Start (1 bit)

2)			Master:	Adress of a slave (7/10 bits)

3)			Master:	R/W (Receiving) (1 bit)
4)			Slave: 	Send ACK/NACK (1 bit)

5)			Master decides to recieve data from a slave

  5.1.1)		Slave:	Data frame 1 (8 bits)
  5.1.2)		Master:	Send ACK/NACK (1 bit)
  
  5.2.1)		Slave:	Data frame 2 (8 bits)
  5.2.2)		Master:	Send ACK/NACK (1 bit)
  
  5.n.1)		Slave:	Data frame n (8 bits)
  5.n.2)		Master:	Send ACK/NACK (1 bit)

6)			Master:	Stop (1 bit)
</pre>

---

