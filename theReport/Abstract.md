# Abstract

The main aim of this project has been to realize the concept of a fully autonomous wheel loader called the "Volvo Concept Wheel Loader ZEUX" on its LEGO prototype called the "LEGO Technic 42081 Volvo Concept Wheel Loader ZEUX".
 
At the beginning of this project, a fully assembled LEGO prototype of the Volvo ZEUX was all that was. There were neither actuators nor sensors (and no micro controller- or processer unit either) in/on it. After approximately three months of hard work, the final result of this project ended up being a modified LEGO prototype of the Volvo ZEUX wheel loader that is remote controlled using an "XBOX 360 controller", not as an RC (Radio-Controlled) car but in the following procedure: radio signals from the "XBOX 360 controller" get received by an "XBOX 360 PC Wireless Gaming Receiver" which is an input into an Ubuntu desktop that communicates on ROS (Robotics Operating System) via Wi-Fi with a Raspberry-pi 4 that is mounted on the wheel loader. The Raspberry-pi communicates with two Nucleo STM-32 micro controllers through USB-cables and with the current sensors (that are there to tell how much load that is being scooped) using its built-in I2C interface. The micro controllers send commands to the actuators and receive feedback data from them. 

For the wheel loader to be fully autonomous, it needs to be connected to some kind of artificial intelligence which in this case is a machine learning algorithm developed as a PHD project at LTU. This machine learning algorithm is trained on scooping on a full sized wheel loader. 

The goal of having a fully autonomous wheel loader is not realized in this project. But most (if not all) the steps needed to be taken previous to this step are completed. So replacing the "XBOX 360 controller" and the "XBOX 360 PC Wireless Gaming Receiver" with the machine learning algorithm (and adapt the machine learning algorithm to this wheel loader and probably some time demanding trouble shootings) gives a fully autonomous wheel loader. This last mentioned step is not taken in this project due to time constraint.



