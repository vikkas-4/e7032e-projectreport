/* Program inspired from Adafruits example code fot the "Adafruit INA219" */
//Install library:  Sketch --> Include Library --> Manage Library --> install "Adafruit INA219"

#include <Wire.h>
#include <Adafruit_INA219.h> //Library from Adafruit
Adafruit_INA219 ina219;

//#include <INA219.h> //Library not from Adafruit
//INA219 monitor;  //Library not from Adafruit

void setup(void) {
  Serial.begin(9600);
  while (!Serial) {   // will pause Zero, Leonardo, etc until serial console opens
    delay(1);
  }

  uint32_t currentFrequency;

  Serial.println("Hello!");
  ina219.begin();
  ina219.setCalibration_16V_400mA();
  Serial.println("Measuring voltage and current with INA219 ...");
}

void loop(void) {
  float shuntvoltage = 0;
  float busvoltage = 0;
  float current_mA = 0;
  float loadvoltage = 0;
  float power_mW = 0;

  shuntvoltage = ina219.getShuntVoltage_mV();
  busvoltage = ina219.getBusVoltage_V();
  current_mA = ina219.getCurrent_mA();
  power_mW = ina219.getPower_mW();
  loadvoltage = busvoltage + (shuntvoltage / 1000);

  Serial.print("Bus Voltage:   "); Serial.print(busvoltage); Serial.println(" V");
  Serial.print("Shunt Voltage: "); Serial.print(shuntvoltage); Serial.println(" mV");
  Serial.print("Load Voltage:  "); Serial.print(loadvoltage); Serial.println(" V");
  Serial.print("Current:       "); Serial.print(current_mA); Serial.println(" mA");
  Serial.print("Power:         "); Serial.print(power_mW); Serial.println(" mW");
  Serial.println("");

  delay(2000);
}


// Outputs...

/*
  V_in= 5V
  A_in= 1.8A = 1800 mA
     Vcc = 3.3V (power supply to INA219)
  Calibration: 16V_400mA
  
  ======================
  R = 0 ohm
  ----------------------
  Hello!
  Measuring voltage and current with INA219 ...
Bus Voltage:   0.00 V
Shunt Voltage: 0.02 mV
Load Voltage:  0.00 V
Current:       0.20 mA
Power:         0.00 mW

Bus Voltage:   0.00 V
Shunt Voltage: 0.02 mV
Load Voltage:  0.00 V
Current:       0.20 mA
Power:         0.00 mW

Bus Voltage:   0.28 V
Shunt Voltage: 40.00 mV
Load Voltage:  0.32 V
Current:       400.00 mA
Power:         115.00 mW

Bus Voltage:   0.00 V
Shunt Voltage: 0.43 mV
Load Voltage:  0.00 V
Current:       3.30 mA
Power:         0.00 mW
 ...

  ======================
  R = 10 ohm
  ----------------------
  Hello!
  Measuring voltage and current with INA219 ...
Bus Voltage:   0.02 V
Shunt Voltage: 40.00 mV
Load Voltage:  0.06 V
Current:       400.00 mA
Power:         10.00 mW

Bus Voltage:   0.02 V
Shunt Voltage: 40.00 mV
Load Voltage:  0.06 V
Current:       400.00 mA
Power:         10.00 mW

Bus Voltage:   0.02 V
Shunt Voltage: 40.00 mV
Load Voltage:  0.06 V
Current:       400.00 mA
Power:         10.00 mW

Bus Voltage:   0.02 V
Shunt Voltage: 40.00 mV
Load Voltage:  0.06 V
Current:       400.00 mA
Power:         10.00 mW
...

  ======================
  R = 20 ohm
  ----------------------
  Hello!
  Measuring voltage and current with INA219 ...
Bus Voltage:   0.01 V
Shunt Voltage: 26.00 mV
Load Voltage:  0.04 V
Current:       260.00 mA
Power:         3.00 mW

Bus Voltage:   0.01 V
Shunt Voltage: 26.00 mV
Load Voltage:  0.03 V
Current:       260.10 mA
Power:         3.00 mW

Bus Voltage:   0.01 V
Shunt Voltage: 26.01 mV
Load Voltage:  0.04 V
Current:       260.00 mA
Power:         3.00 mW

Bus Voltage:   0.01 V
Shunt Voltage: 26.00 mV
Load Voltage:  0.04 V
Current:       260.00 mA
Power:         3.00 mW
  ...

  ======================
  R = 30 ohm
  ----------------------
  Hello!
  Measuring voltage and current with INA219 ...
Bus Voltage:   0.00 V
Shunt Voltage: 17.65 mV
Load Voltage:  0.02 V
Current:       176.50 mA
Power:         0.00 mW

Bus Voltage:   0.00 V
Shunt Voltage: 17.64 mV
Load Voltage:  0.02 V
Current:       176.50 mA
Power:         0.00 mW

Bus Voltage:   0.00 V
Shunt Voltage: 17.64 mV
Load Voltage:  0.02 V
Current:       176.40 mA
Power:         0.00 mW

Bus Voltage:   0.01 V
Shunt Voltage: 17.64 mV
Load Voltage:  0.03 V
Current:       176.40 mA
Power:         0.00 mW
 ...

  ======================
  R = inf. ohm (No input)
  ----------------------
Hello!
Measuring voltage and current with INA219 ...
Bus Voltage:   0.00 V
Shunt Voltage: 0.02 mV
Load Voltage:  0.00 V
Current:       0.20 mA
Power:         0.00 mW

Bus Voltage:   0.00 V
Shunt Voltage: 0.01 mV
Load Voltage:  0.00 V
Current:       0.10 mA
Power:         0.00 mW

Bus Voltage:   0.00 V
Shunt Voltage: 0.02 mV
Load Voltage:  0.00 V
Current:       0.10 mA
Power:         0.00 mW

Bus Voltage:   0.00 V
Shunt Voltage: 0.01 mV
Load Voltage:  0.00 V
Current:       0.10 mA
Power:         0.00 mW

*/


  //======================
  //======================
  //======================

/*
  V_in= 5V
  A_in= 1.8A = 1800 mA
      Vcc = 5 V (power supply to INA219)
  Calibration: 16V_400mA
  
  ======================
  R = 0 ohm
  ----------------------
Hello!
Measuring voltage and current with INA219 ...
Bus Voltage:   0.00 V
Shunt Voltage: 0.03 mV
Load Voltage:  0.00 V
Current:       0.30 mA
Power:         0.00 mW
...

  ======================
  R = 10 ohm
  ----------------------
Hello!
Measuring voltage and current with INA219 ...
Bus Voltage:   0.00 V
Shunt Voltage: 40.00 mV
Load Voltage:  0.04 V
Current:       400.00 mA
Power:         0.00 mW
...

  ======================
  R = 20 ohm
  ----------------------
Hello!
Measuring voltage and current with INA219 ...
Bus Voltage:   0.00 V
Shunt Voltage: 26.20 mV
Load Voltage:  0.03 V
Current:       262.00 mA
Power:         0.00 mW
...

  ======================
  R = 30 ohm
  ----------------------
Hello!
Measuring voltage and current with INA219 ...
Bus Voltage:   0.00 V
Shunt Voltage: 17.72 mV
Load Voltage:  0.02 V
Current:       177.10 mA
Power:         0.00 mW
...

  ======================
  R = inf. ohm (No input)
  ----------------------
Bus Voltage:   0.00 V
Shunt Voltage: -0.01 mV
Load Voltage:  -0.00 V
Current:       0.00 mA
Power:         0.00 mW
...
*/




















