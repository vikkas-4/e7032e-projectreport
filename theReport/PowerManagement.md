## Power management 

All the electronics situated on the wheel loader needs power. This power comes from a 3-cell lithium-ion battery with a nominal voltage of 11.1V and a nominal capacity of 3450mAh. The power from the battery is then converted to the correct voltages and distributed to the electronics. Three different boards are used for DC-DC conversion, all three buck converters.

The first board, pictured below, has an input voltage of 6-40V and an output voltage of 5V through two USB type-A ports. One of the output ports is rated at 1A and the other at 2A. This DC-DC converter is used to power the Raspberry Pi through the 2A port using a USB type-A to USB type-C cable.

The Raspberry Pi is recommended to be used with a power supply rated at 2.5-3A but this is to ensure that there will be no problems during power-hungry tasks with power-hungry peripherals attached. Since we will not have very power-hungry electronics attached and will be performing easy tasks on the Raspberry Pi this should not be a problem.

From the Raspberry Pi we are powering both of the Nucleo boards using the USB cable that is also used to send data between the boards. The Nucleo boards can draw a maximum of 300mA each. The Raspberry Pi/Nucleos also power the logic for the encoders and the current sensors.

<figure><img src="pictures/USBconverter.png" width="450"><figcaption>5V USB DC-DC converter <figcaption></figure>

The second board, pictured below, has an input voltage of 7-28V and an adjustable output voltage. It has a rated output current of 1.5A continuously and 3A maximum. We are using three of these tuned to 5V to power the logic on the motor controllers.

<figure><img src="pictures/5Vconverter.png" width="450"><figcaption>5V DC-DC converter <figcaption></figure>

The third board, pictured below, has an input voltage of 8-36V and an adjustable output voltage. It has a rated output current of 5A continuously and will automatically shut off at 8A. It is also rated at 75W of output power and will automatically shut off if it gets too hot.


Four of these converter boards are used, all of them tuned to 6V. One of the converter boards is used for the steering servo which has a maximum current draw of about 3A. The other three converter boards are used to power two of the motors each. The motors have a maximum current draw of 3.2A each when stalled, so a maximum of 6.4A (38.4W) per converter board. This is more than the continuously rated output of the board, but since we probably are not going to use full power at stall with both motors for any longer periods this should not be a problem.

<figure><img src="pictures/6Vconverter.png" width="450"><figcaption>6V DC-DC converter <figcaption></figure>

All of these DC-DC converter boards needs to be connected to the
battery. For this we are using a power distribution board, see picture below, where all eight converter boards can be connected. The battery is connected to the power distribution board with a terminal strip and a switch is used to power everything on and off easily.

<figure><img src="pictures/PowerDistribution.jpg" width="250"><figcaption>Power distribution board <figcaption></figure>

Pictured below are some simple schematics of how the power is connected and distributed.

<figure><img src="pictures/PowerToRPi.png" width="350"><figcaption>Power to the Raspberry Pi <figcaption></figure>

<figure><img src="pictures/PowerToMotorController.png" width="350"><figcaption>Power to motor controller <figcaption></figure>

<figure><img src="pictures/PowerToServo.png" width="350"><figcaption>Power to the servo <figcaption></figure>