## AI

Since we do not have access to, and would not have time to implement, the AI created in the doctoral thesis we created our own \"fake AI\". This AI is used to simulate the real AI, to see so that it would be compatible and work in our system.

The fake AI uses an Xbox 360 controller as input to get all the axes needed. The input range of the axes is from -1 to 1, just like the output from the real AI. The axes are input directly to python using the _pygame_ package. They are then scaled and sent over the ROS network to the wheel loader.