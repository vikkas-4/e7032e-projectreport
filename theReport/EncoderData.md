# Encoder Data

## Speed

Encoders are used for calculating the speed of the motors turning. The code for this is shown below.

First the encoder is triggered on the pulses for the encoders on the motors and stores the number of pulses inside a variable, in this case encVal1.

```c
void EXTI0_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI0_IRQn 0 */
  if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0)) encVal1++;

  /* USER CODE END EXTI0_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
  /* USER CODE BEGIN EXTI0_IRQn 1 */

  /* USER CODE END EXTI0_IRQn 1 */
}
```

Then the data is sent over to the main script where calculating the speed is done and finally transmitted back to the RasPI. The drive speed values are taken as a mean of all the motor encoder values but the code for the arm and bucket works in the same way.

```c
currentTime = HAL_GetTick();
  if((currentTime - previousTime) > interval){
   previousTime = currentTime;

   // Calculate the speed
   rpm_M1 = encVal1*60/ENC_REV_COUNT;
   rpm_M2 = encVal2*60/ENC_REV_COUNT;
   rpm_M3 = encVal3*60/ENC_REV_COUNT;
   rpm_M4 = encVal4*60/ENC_REV_COUNT;

   // Calculate average speed
   driveSensorData[2] = (rpm_M1 + rpm_M2 + rpm_M3 + rpm_M4)/4;

   // Reset encoder values
   encVal1 = 0;
   encVal2 = 0;
   encVal3 = 0;
   encVal4 = 0;

   // Send sensor data
   HAL_UART_Transmit(&huart2, driveSensorData, 3, 10);
   }
```

## Position

Position is also a desired sensor data when talking about the arm and bucket. 

The position is a bit more complicated to calculated than the speed. It requires data from 2 encoder inputs which are compared to each other. If they are the same the position counter is incremented and vice verse when they are different. This way we can know which way the motor is turning and the number of pulses it has traveled in a certain direction. the only thing to keep in mind is that the zero position varies when the wheel loader is restarted. The arm and bucket needs to be put in a location which is know to be zero position. A fix to this problem would be to implement a homing feature much like the ones used on 3D-Printer.

```c
void EXTI0_IRQHandler(void)
{
  /* USER CODE BEGIN EXTI0_IRQn 0 */	
	// For speed Calculation
	if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0)) encVal_ArmA++;
	
    // Position
	if (HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_1))
  	{
		encArmPos++;
  	}
    else
    {
    	encArmPos--;
    }

  /* USER CODE END EXTI0_IRQn 0 */
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
  /* USER CODE BEGIN EXTI0_IRQn 1 */

  /* USER CODE END EXTI0_IRQn 1 */
}
```

