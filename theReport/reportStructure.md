# Report structure

* IMR(A)D
* [Report and expectations](https://ltu.instructure.com/courses/7896/pages/report-and-expectations)

* Use Grammarly and Microsoft Readability Statistics.
* __Use proper verb tense for the reader at the time of reading.__


## Abstract

* Summarize main points of the report

* Include result of project and discussion 

* The reader should have a good idea of what he/she is reading after having read the abstract.

## Table of contents

* Well defined headers

* Easily understood

## Introduction

* Project description
  
  * nature
  
  * scope
  
  * Project goals

* Project background
  
  * define where the project started
  
  * why are we doing this project

## Method

* Planning the project

* Workflow

* Software
  
  * Programs used
    
    * Arduino IDE, etc.
  
  * Source code design
    
    * Program flow
  
  * Communication protocols
    
    * Serial
    
    * I2C
    
    * ROS
    
    * SSH
  
  * Overall program flow

* Hardware
  
  * The lego wheelloader
    
    * measurements, etc.
  
  * Electronics used
    
    * Current measurment module
    
    * Motors
    
    * Encoders
    
    * Arduino
    
    * RasPI
  
  * System design
    
    * Why and how?
    
    * Coupling sheet (how eveything is connceted)

* Control
  
  * Design of algorithms
    
    * PID, etc.
  
  * Modelling
    
    * Physical models
    
    * Estimation model current to lift
    
    

## Result

* Simulation result of control algorithms

* Tests of the real system
  
  * measurments vs reality

* Current measurment estimation lift/tilt force
  
  * simulation
  
  * Measurments vs reality

## Discussion

* What questions have arised during the project
  
  * What can be improved?
  
  * What should Volvo think about when developing the wheel loader in full scale?

* How the project went

* future research
