# Structure for the MidTerm Presentation

## Introduction
* Present the group
* Present what we are going to talk about

## Background
* Background to the project
    * info...
* Planning the project
    * Gantt
    * Trello
    * Telegram

## Design choices
* Connection diagrams
    * Pinout and so on
* Flowcharts
    * who talks to who
* Hardware choices
    * ~~Arduino vs STM32?~~
        * ~~Interrupts...~~
        * ~~Harder to use STM32~~
        * ~~Use two STM32 for arm and drive?~~
        Känns konstigt i en presentation för helt nyinvigda, kanske nämner att vi valde STM32:an för tillgång till interruts.
    * Why use RasPi?
        * Networking
        * ROS
    * Actuators
        * Motors
    * Sensors
        * Encoders
        * Current sensor
* Control strategy
    * Block diagrams
    * Design choices
    * Equations
* Programming choices
    * C-code vs Arduino code
    * CubeMX
    * Structure
* Communication
    * I2C
    * USART

## Finish
* Finish the presentation by saying thank you for your time.
