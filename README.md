# E7032E - Project Report

This is the project report git repository for the course E7032E at Luleå University of Technology. 




## Part 1 - The Report

* [Abstract](theReport/Abstract.md)
* [Introduction](theReport/Introduction.md)
* Method
	* [Planning the Project](theReport/planning.md)
	* [Choosing components](theReport/choosingComponents.md)
	* [Electronics](theReport/electronics.md)
	* [Connecting the electronics](theReport/Pinout.md)
	* [Power management](theReport/PowerManagement.md)
	* [3D-print design](theReport/MountMethod.md)
	* [Code strucure](theReport/codeStructure.md)
	* [ROS](theReport/ros.md)
* [AI](theReport/AI.md)
* Results
	- Code
		+ [Communication](theReport/communication.md)
		+ [Encoder Data](theReport/EncoderData.md)
		+ [Drive and steering](theReport/drive.md)
		+ [Arm and Bucket](theReport/arm.md)
		+ [STM32 Data Transceiving](theReport/DataTransceiving.md)
	- [Printed Mounts](theReport/MountResult.md)
* Discussion
	- [Current sensors](theReport/The-Current-Sensor.md)
	- [Redesign](theReport/EvoResult.md)
	- [Limitations of the code](theReport/codeLim.md)
	- [Project planning and future work](theReport/Discussion.md)
* [References]()


## Part 2 - Appendices

### Gantt
* [Initial gantt](Appendices/Gantt/Gantt_Whole_Project.pdf)

* [Post Gantt](Appendices/Gantt/PostGantt_Whole_Project.pdf)

### Responsibilities

Below is a table which shows the different responsibility areas of the different members within the group.

| Names  | Control | Hardware | Software | Git | Project management |
| ------ |:-------:|:--------:|:--------:|:---:|:------------------:|
| Viktor | x       |          |          | x   | x                  |
| Martin | x       | x        |          |     |                    |
| Emil   | x       |          | x        |     |                    |
| Adonay |         | x        | x        |     |                    |
| Linus  | x       |          | x        |     |                    |

#### People

[Viktor				(Student)](Appendices/People/Viktor.md)


[Martin				(Student)](Appendices/People/Martin.md)

[Emil				(Student)](Appendices/People/Emil.md)

[Adonay				(Student)](Appendices/People/Adonay.md)

[Linus				(Student)](Appendices/People/Linus.md)

[Jan van Deventer	(Supervisor)](https://www.ltu.se/staff/d/deventer-1.9887?l=en)

### Workflow and Development Strategies

* [Overall Project Flow](Appendices/overallProjectFlowV2.png)
* [Git Workflow](Appendices/gitworkflow.md)
* [Project planning and workflow](Appendices/planning.md)

### [System Specification and Requirements](Appendices/spec_and_req.md)

The link below redirects to a repository which holds more information on how everything should be used and some useful examples on how different components work. 


[https://gitlab.com/vikkas-4/e7032e-legowheelloader](https://gitlab.com/vikkas-4/e7032e-legowheelloader)


### Good links

- [Full report structure](theReport/reportStructure.md)
- [Arduino Motor speed control(mathematical model)](https://www.thepoorengineer.com/en/motor-speed-control/#model)
- [Communication protocol(Ras-Ard)](https://medium.com/@araffin/simple-and-robust-computer-arduino-serial-communication-f91b95596788)
- [Equation maker](https://www.codecogs.com/latex/eqneditor.php)
- [Interrupts](https://stm32f4-discovery.net/2014/08/stm32f4-external-interrupts-tutorial/)
